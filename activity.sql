[--Create Records--]
INSERT INTO users (email, password, datetime_created) VALUES ("johnsmith@gmail.com","passwordA","2021-1-1 1:00:00");
INSERT INTO users (email, password, datetime_created) VALUES ("juandelacruz@gmail.com","passwordB","2021-1-1 2:00:00");
INSERT INTO users (email, password, datetime_created) VALUES ("janesmith@gmail.com","passwordC","2021-1-1 3:00:00");
INSERT INTO users (email, password, datetime_created) VALUES ("mariadelacruz@gmail.com","passwordD","2021-1-1 4:00:0");
INSERT INTO users (email, password, datetime_created) VALUES ("johndoe@gmail.com","passwordE","2021-1-1 5:00:00");

INSERT INTO posts (user_id, title, content, datetime_posted) VALUES (1,"First Code","Hello World!","2021-1-2 1:00:00");
INSERT INTO posts (user_id, title, content, datetime_posted) VALUES (1,"Second Code","Hello Earth!","2021-1-2 2:00:00");
INSERT INTO posts (user_id, title, content, datetime_posted) VALUES (2,"Third Code","Welcome to Mars!","2021-1-2 3:00:00");
INSERT INTO posts (user_id, title, content, datetime_posted) VALUES (4,"Fourth Code","Bye bye Solar System","2021-1-2 4:00:00");

[--Read--]
SELECT * FROM posts WHERE user_id = 1;
SELECT email, datetime_created FROM users;

[--Update--]
UPDATE posts SET content = "Hello to the people of the Earth!" WHERE id = 2;

[--Delete--]
DELETE FROM users WHERE email = "johndoe@gmail.com";